package com.example.student.twitter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class NotifsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifs);

        NotifsFragment notifs = new NotifsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, notifs).commit();
    }
}
