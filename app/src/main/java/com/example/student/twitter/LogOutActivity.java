package com.example.student.twitter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LogOutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_out);

        LogOutFragment logOut = new LogOutFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.logOutContainer, logOut).commit();
    }
}
