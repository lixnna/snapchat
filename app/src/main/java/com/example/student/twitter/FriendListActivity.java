package com.example.student.twitter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FriendListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_menu);

        FriendListFragment friendsList = new FriendListFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.friendsListContainer, friendsList).commit();
    }
}
