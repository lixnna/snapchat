package com.example.student.twitter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "313A08EE-95FB-7C70-FF1B-5F5208426F00";
    public static final String SECRET_KEY = "177DAA9A-B633-530C-FF38-A3B9EC21E700";
    public static final String VERSION = "v1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if (Backendless.UserService.loggedInUser() == "") {
            LoginMenuFragment loginMenu = new LoginMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, loginMenu).commit();
        } else {
            FriendsMenuFragment friends = new FriendsMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, friends).commit();
        }
    }
}