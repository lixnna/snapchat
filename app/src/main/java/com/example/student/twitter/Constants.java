package com.example.student.twitter;

public class Constants {

    public static final String ACTION_ADD_FRIEND = "com.example.student.twitter.ADD_FRIEND";
    public static final String ACTION_SEND_FRIEND_REQUEST = "com.example.student.twitter.SEND_FRIEND_REQUEST";
    public static final String ACTION_SELECT_PICTURE = "com.example.student.twitter.ACTION_GET_CONTENT";
    public static final String ACTION_SEND_PHOTO = "com.example.student.twitter.SEND_PHOTO";

    public static final String BROADCAST_ADD_FRIEND_SUCCESS = "com.example.student.twitter.ADD_FRIEND_SUCCESS";
    public static final String BROADCAST_ADD_FRIEND_FAILURE = "com.example.student.twitter.ADD_FRIEND_FAILURE";

    public static final String BROADCAST_FRIEND_REQUEST_SUCCESS = "com.example.student.twitter.FRIEND_REQUEST_SUCCESS";
    public static final String BROADCAST_FRIEND_REQUEST_FAILURE = "com.example.student.twitter.FRIEND_REQUEST_FAILURE";

}
