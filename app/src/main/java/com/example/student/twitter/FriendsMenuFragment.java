package com.example.student.twitter;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class FriendsMenuFragment extends Fragment {

    static final int REQUEST_CHOOSE_PHOTO = 2;


    public FriendsMenuFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_friends_menu, container, false);

        String[] menuItems = {"Camera",
                "Send Picture",
                "Incoming Pictures",
                "Friends",
                "Add Friend",
                "Incoming Friend Requests",
                "Log Out"};

        ListView listView = (ListView) view.findViewById(R.id.listView);

        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                menuItems
        );

        listView.setAdapter(listViewAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Intent intent = new Intent(getActivity(), CameraActivity.class);
                    startActivity(intent);
                } else if (position == 1) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CHOOSE_PHOTO);
                } else if (position == 2) {
                    Intent intent = new Intent(getActivity(), InboxActivity.class);
                    startActivity(intent);
                }else if (position == 3) {
                    Intent intent = new Intent(getActivity(), FriendListActivity.class);
                    startActivity(intent);
                } else if (position == 4) {
                    Intent intent = new Intent(getActivity(), AddFriendActivity.class);
                    startActivity(intent);
                } else if (position == 5) {
                    Intent intent = new Intent(getActivity(), FriendRequestActivity.class);
                    startActivity(intent);
                } else if (position == 6) {
                    Intent intent = new Intent(getActivity(), LogOutActivity.class);
                    startActivity(intent);
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CHOOSE_PHOTO){
            if(resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                Intent intent = new Intent(getActivity(), FriendListActivity.class);
                intent.putExtra("ImageURI", uri);
                startActivity(intent);
            }
        }
    }
}
