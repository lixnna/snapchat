package com.example.student.twitter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FriendRequestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_request);

        FriendRequestFragment friendRequest = new FriendRequestFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.friendRequestContainer, friendRequest).commit();
    }
}
